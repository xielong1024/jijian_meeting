/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : jijian_meeting

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-01-25 15:41:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_resource`
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(256) DEFAULT NULL COMMENT '资源名称',
  `status` varchar(4) DEFAULT NULL COMMENT '资源状态',
  `create_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_resource
-- ----------------------------
INSERT INTO `tb_resource` VALUES ('1', '会议室1', '1', '2018-01-22 17:24:56');
INSERT INTO `tb_resource` VALUES ('2', '会议室2', '1', '2018-01-23 14:58:02');

-- ----------------------------
-- Table structure for `tb_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `tb_schedule`;
CREATE TABLE `tb_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(4) DEFAULT NULL COMMENT '分上下午（AM,PM）',
  `day` int(11) DEFAULT NULL COMMENT '记录周几',
  `book_time` varchar(256) DEFAULT NULL COMMENT '预约日期',
  `meeting_name` varchar(256) DEFAULT NULL COMMENT '会议名称',
  `resource_id` int(11) DEFAULT NULL COMMENT '资源ID',
  `resource_name` varchar(256) DEFAULT NULL COMMENT '会议室名称',
  `book_userid` int(11) DEFAULT NULL COMMENT '预约人',
  `user_name` varchar(256) DEFAULT NULL COMMENT '预约人',
  `order_book_date` date DEFAULT NULL COMMENT '排序预约时间',
  `create_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_schedule
-- ----------------------------
INSERT INTO `tb_schedule` VALUES ('2', 'AM', '7', '2018-01-28', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-28', '2018-01-28 14:45:10');
INSERT INTO `tb_schedule` VALUES ('10', 'AM', '1', '2018-01-22', '广州局基建会议室功能开发会议', '2', '会议室2', null, 'Alex', '2018-01-22', '2018-01-22 14:42:13');
INSERT INTO `tb_schedule` VALUES ('11', 'AM', '2', '2018-01-23', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-23', '2018-01-23 12:50:14');
INSERT INTO `tb_schedule` VALUES ('12', 'PM', '2', '2018-01-23', '广州局基建会议室功能开发会议', '2', '会议室2', null, 'Alex', '2018-01-23', '2018-01-23 10:48:17');
INSERT INTO `tb_schedule` VALUES ('13', 'PM', '3', '2018-01-24', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-24', '2018-01-23 10:48:23');
INSERT INTO `tb_schedule` VALUES ('14', 'AM', '3', '2018-01-24', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-24', '2018-01-23 10:48:31');
INSERT INTO `tb_schedule` VALUES ('15', 'AM', '4', '2018-01-25', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-25', '2018-01-23 10:48:37');
INSERT INTO `tb_schedule` VALUES ('16', 'PM', '4', '2018-01-25', '广州局基建会议室功能开发会议', '2', '会议室2', null, 'Alex', '2018-01-25', '2018-01-23 10:48:45');
INSERT INTO `tb_schedule` VALUES ('17', 'AM', '1', '2018-01-29', '广州局基建会议室功能开发会议', '2', '会议室2', null, 'Alex', '2018-01-29', '2018-01-29 14:43:46');
INSERT INTO `tb_schedule` VALUES ('19', 'PM', '2', '2018-01-23', '广州局基建会议室功能开发会议', '1', '会议室1', null, 'Alex', '2018-01-23', '2018-01-23 16:40:50');
INSERT INTO `tb_schedule` VALUES ('20', 'AM', '4', '2018-01-25', '广州局基建会议室功能开发会议', '2', '会议室2', null, 'Alex', '2018-01-25', '2018-01-25 09:35:09');
INSERT INTO `tb_schedule` VALUES ('29', 'PM', '7', '2018-01-28', '年终奖会议2', '1', '会议室1', null, '4', '2018-01-28', '2018-01-25 13:43:15');
INSERT INTO `tb_schedule` VALUES ('30', 'PM', '1', '2018-01-22', '年终奖会议2', '1', '会议室1', null, '4', '2018-01-22', '2018-01-25 13:43:49');
INSERT INTO `tb_schedule` VALUES ('31', 'PM', '3', '2018-01-31', '年终奖会议2', '1', '会议室1', null, '4', '2018-01-31', '2018-01-25 13:44:38');

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(256) DEFAULT NULL COMMENT '用户账号',
  `password` varchar(256) DEFAULT NULL COMMENT '用户密码',
  `user_name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'admin', '202cb962ac59075b964b07152d234b70', '管理员');
INSERT INTO `tb_user` VALUES ('2', 'zhangsan', '123', '张三');
