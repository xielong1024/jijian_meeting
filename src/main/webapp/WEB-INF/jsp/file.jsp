<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<body>
<c:forEach items="${data.data}" var="item">
    <div>
        文件名:<c:out value="${item.fileName}"/><p>
        文件路径:<c:out value="${item.fileUrl}"/><p>
    </div>
</c:forEach>
</body>

</html>