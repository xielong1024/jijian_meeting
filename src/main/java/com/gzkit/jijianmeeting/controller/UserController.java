package com.gzkit.jijianmeeting.controller;

import com.alibaba.fastjson.JSON;
import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.JkReturnJson;
import com.gzkit.jijianmeeting.common.util.MD5Util;
import com.gzkit.jijianmeeting.dao.UserMapper;
import com.gzkit.jijianmeeting.entity.UserEntity;
import com.gzkit.jijianmeeting.service.UserService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Controller
@Scope(value = "prototype")
@RequestMapping(value = "/user")
public class UserController {
    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "userMapper")
    private UserMapper userMapper;

    /**
     * 用户登录
     *
     * @param account
     * @param password
     * @return
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public JkReturnJson login(String account, String password,HttpServletRequest request) {
        JkReturnJson json = new JkReturnJson();
        String message = "用户登录成功";
        boolean isExist = userService.login(account, password,request);
        if (!isExist) {
           message="用户登录失败";
           json.setStatusCode("0001");
        }else {

        }
        json.setUserMsg(message);
        return json;
    }

    @RequestMapping(value = "/logout")
    @ResponseBody
    public JkReturnJson logout(HttpServletRequest request){
        JkReturnJson json = new JkReturnJson();
        String message = "用户注销成功";
        try{
        request.getSession().invalidate();
        }catch(Exception e){
            message="用户注销失败";
            e.printStackTrace();
        }
        json.setUserMsg(message);
        return json;
    }

    @RequestMapping(value = "/addUser")
    @ResponseBody
    public JkReturnJson addUser(UserEntity userEntity) {
        JkReturnJson json = new JkReturnJson();
        String message = "用户添加成功";
        try {
            boolean account=userService.isExistUser(userEntity.getAccount());
            if (!account){
            String pw = MD5Util.getMD5(userEntity.getPassword(), "", 0);
            userEntity.setPassword(pw);
            userMapper.insert(userEntity);
            }else{
                message="该用户已存在";
                json.setStatusCode("0001");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        json.setUserMsg(message);
        return json;
    }

    @RequestMapping(value = "/deleteUser")
    @ResponseBody
    public JkReturnJson deleteUser(Integer id){
        JkReturnJson json=new JkReturnJson();
        String message = "用户删除成功";
        try{
            userMapper.deleteByPrimaryKey(id);
        }catch(Exception e){
            message = "用户删除失败";
            e.printStackTrace();
        }
        json.setUserMsg(message);
        return json;
    }
    @RequestMapping(value = "/getList")
    @ResponseBody
    public JkReturnJson getUserList(HttpServletRequest request){
        JkReturnJson json=new JkReturnJson();
        String message="获取列表成功";
        try{
            String pageSize=request.getParameter("page");
            String rowsSize=request.getParameter("rows");
            int page=pageSize==null?1:Integer.valueOf(pageSize);
            int rows=rowsSize==null?10:Integer.valueOf(rowsSize);
            PageInfos list=userService.getUserEntities(page,rows);
            json.setData(list);
        }catch(Exception e){
            message="获取列表失败";
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return  json;
    }

    @RequestMapping(value = "/index")
    public ModelAndView index(){
        return new ModelAndView("login");
    }

}
