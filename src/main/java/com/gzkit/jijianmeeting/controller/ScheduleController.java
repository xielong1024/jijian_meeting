package com.gzkit.jijianmeeting.controller;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.JkReturnJson;
import com.gzkit.jijianmeeting.dao.ResourceMapper;
import com.gzkit.jijianmeeting.dao.ScheduleMapper;
import com.gzkit.jijianmeeting.entity.ResourceEntity;
import com.gzkit.jijianmeeting.entity.ScheduleEntity;
import com.gzkit.jijianmeeting.service.ScheduleService;
import com.gzkit.jijianmeeting.service.impl.ScheduleServiceImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Controller
@Scope(value = "prototype")
@RequestMapping(value = "/schedule")
public class ScheduleController {
    private static  final  SimpleDateFormat sfd=new SimpleDateFormat("yyyy-MM-dd");
    @Resource(name = "scheduleService")
    private ScheduleService scheduleService;

    @Resource(name = "scheduleMapper")
    private ScheduleMapper scheduleMapper;

    @Resource(name = "resourceMapper")
    private ResourceMapper resourceMapper;

    @RequestMapping(value = "/getDates")
    @ResponseBody
    public JkReturnJson getDates(){
        JkReturnJson json=new JkReturnJson();
        List<Map<String,Object>> dates=scheduleService.getDates(new Date());
        json.setData(dates);
        return json;
    }

    /**
     *添加预约
     * @param scheduleEntity
     * @return
     */
    @RequestMapping(value = "/addSchuedule")
    @ResponseBody
    public JkReturnJson addSchuedule(ScheduleEntity scheduleEntity,ResourceEntity resourceEntity){
        JkReturnJson json=new JkReturnJson();
        String message="添加成功";
        try{
            String time=scheduleEntity.getBookTime();
            String zone=scheduleEntity.getZone();
            Integer resourceId=scheduleEntity.getResourceId();
            boolean isExist=scheduleService.isExistBookTime(time,zone,resourceId);
            if (!isExist){
                Date date=new Date();
                scheduleEntity.setCreateDate(date);
                scheduleEntity.setOrderBookDate(sfd.parse(scheduleEntity.getBookTime()));
                scheduleEntity.setDay(ScheduleServiceImpl.getWeekDay(scheduleEntity.getBookTime()));
                resourceEntity.setId(scheduleEntity.getResourceId());
                ResourceEntity rEntity=resourceMapper.selectOne(resourceEntity);
                scheduleEntity.setResourceName(rEntity.getResourceName());
                scheduleMapper.insert(scheduleEntity);
                date=null;
            }else{
                json.setStatusCode("0001");
                message="该时间段已被预约";
            }
        }catch(Exception e){
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return json;
    }


    @RequestMapping(value = "/getList")
    @ResponseBody
    public JkReturnJson getList(HttpServletRequest request,ScheduleEntity scheduleEntity){
        JkReturnJson json=new JkReturnJson();
        String message="获取列表成功";
        try{
            String pageSize=request.getParameter("page");
            String rowsSize=request.getParameter("rows");
            int page=pageSize==null?1:Integer.valueOf(pageSize);
            int rows=rowsSize==null?10:Integer.valueOf(rowsSize);
            PageInfos pageInfos=scheduleService.getList(page,rows,scheduleEntity);
            json.setData(pageInfos);
        }catch(Exception e){
            message="获取列表失败";
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return  json;
    }
    /**
     *取消预约
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteSchedule")
    @ResponseBody
    public JkReturnJson deleteSchedule(Integer id){
        JkReturnJson json=new JkReturnJson();
        String message="删除成功";
        try{
            scheduleMapper.deleteByPrimaryKey(id);
        }catch(Exception e){
            json.setStatusCode("0001");
            message="删除失败";
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return  json;
    }


}
