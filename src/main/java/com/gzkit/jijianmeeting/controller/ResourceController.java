package com.gzkit.jijianmeeting.controller;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.JkReturnJson;
import com.gzkit.jijianmeeting.dao.ResourceMapper;
import com.gzkit.jijianmeeting.entity.ResourceEntity;
import com.gzkit.jijianmeeting.service.ResourceService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Controller
@Scope(value = "prototype")
@RequestMapping(value = "/resource")
public class ResourceController {
    @Resource(name = "resourceMapper")
    private ResourceMapper resourceMapper;

    @Resource(name = "resourceService")
    private ResourceService resourceService;
    /**
     *添加会议室
     * @param resourceEntity
     * @return
     */
    @RequestMapping(value = "/addResource")
    @ResponseBody
    public JkReturnJson addResource(ResourceEntity resourceEntity){
        JkReturnJson json=new JkReturnJson();
        String message="会议室添加成功";
         try{
             boolean resourceName=resourceService.isExistResourceName(resourceEntity.getResourceName());
             if (!resourceName){
                 resourceEntity.setCreateTime(new Date());
                 resourceMapper.insert(resourceEntity);
             }else{
               message="该会议室已存在";
               json.setStatusCode("0001");
             }
         }catch(Exception e){
             message="会议室添加失败";
             e.printStackTrace();
             throw  new RuntimeException(e);
         }
         json.setUserMsg(message);
        return json;
    }

    /**
     *跳转到修改会议室页面
     * @param resourceEntity
     * @param request
     * @return
     */
    @RequestMapping(value = "/goUpdate")
    public String goUpdate(ResourceEntity resourceEntity, HttpServletRequest request){
        ResourceEntity resEntity=resourceMapper.selectOne(resourceEntity);
        request.setAttribute("resourceEntity",resEntity);
        return "test";
    }

    /**
     *修改会议室信息
     * @param resourceEntity
     * @return
     */
    @RequestMapping(value = "/doUpdate")
    @ResponseBody
    public JkReturnJson doUpdate(ResourceEntity resourceEntity){
        JkReturnJson json=new JkReturnJson();
        String message="修改成功";
        boolean resouceName= false ;
        try {
                resourceService.updateResource(resourceEntity);
        } catch (Exception e) {
            json.setStatusCode("0001");
            message="修改失败";
            e.printStackTrace();
        }
        json.setUserMsg(message);
        return json;
    }

    @RequestMapping(value = "/deleteResource")
    @ResponseBody
    public JkReturnJson deleteResource(Integer id){
        JkReturnJson json=new JkReturnJson();
        String message="删除成功";
        try{
           resourceMapper.deleteByPrimaryKey(id);
        }catch(Exception e){
            message="删除失败";
            json.setStatusCode("0001");
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return json;
    }

    /**
     *获取会议室列表
     * @param request
     * @return
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public JkReturnJson getList(HttpServletRequest request){
        JkReturnJson json=new JkReturnJson();
        String message="获取列表成功";
        try{
            String pageSize=request.getParameter("page");
            String rowsSize=request.getParameter("rows");
            int page=pageSize==null?1:Integer.valueOf(pageSize);
            int rows=rowsSize==null?10:Integer.valueOf(rowsSize);
            PageInfos pageInfos=resourceService.getList(page,rows);
            json.setData(pageInfos);
        }catch(Exception e){
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
        json.setUserMsg(message);
        return  json;
    }

    /**
     *获取可用的会议室
     * @return
     */
    @RequestMapping(value = "/getAccessibleResources")
    @ResponseBody
    public JkReturnJson getAccessibleResources(){
        JkReturnJson json=new JkReturnJson();
        String message="获取列表成功";
        try{
            List<ResourceEntity> data=resourceService.getAccessibleResources();
            json.setData(data);
        }catch(Exception e){
            e.printStackTrace();
        }
        return json;
    }

}
