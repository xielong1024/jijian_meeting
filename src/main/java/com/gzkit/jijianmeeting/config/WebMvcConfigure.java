package com.gzkit.jijianmeeting.config;

import com.gzkit.jijianmeeting.common.util.IpUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/26 0026
 */
@Configuration
public class WebMvcConfigure extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/meeting/index.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        super.addViewControllers(registry);
        String os = System.getProperty("os.name");
        if(os.toLowerCase().startsWith("win")){
            Browse("http://localhost:10010/jijian_meeting/meeting/index.html");
        }
    }
    /**
     * @description 启动浏览器跳转到指定url
     * @author hely
     * @date 2017-08-23
     * @param
     */
    public static void Browse(String url) {
        if(Desktop.isDesktopSupported()){
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }else{
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("rundll32 url.dll,FileProtocolHandler " + url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}