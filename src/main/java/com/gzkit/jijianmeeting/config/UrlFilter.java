package com.gzkit.jijianmeeting.config;

import com.gzkit.jijianmeeting.entity.UserEntity;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/26 0026
 */
@WebFilter(filterName = "urlFilter", urlPatterns = "/*")
public class UrlFilter implements Filter {

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        System.out.println("MyFilter init-----------------------------------------");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        UserEntity userEntity = (UserEntity) request.getSession().getAttribute("user");
        String url=request.getRequestURI();
     //   System.out.println(url);
        if(userEntity ==null){
            if(url.contains("index") ||url.contains("login") || !url.contains(".html")){
                chain.doFilter(req, res);
            }else{
                response.sendRedirect("/jijian_meeting/meeting/login.html");
            }
        } else{
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {
        System.out.println("MyFilter destroy");
    }
}