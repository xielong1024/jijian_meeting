package com.gzkit.jijianmeeting.config;


import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * 数据库配置
 */
@EnableTransactionManagement
@AutoConfigureAfter({DataSourceConfig.class})//配置完数据源之后配置
public class DbConfig {
    private static final Logger logger = LoggerFactory.getLogger(DbConfig.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    private Environment env;

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactoryBean sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        //配置mapper路径
//        sqlSessionFactoryBean.setMapperLocations(
//                new PathMatchingResourcePatternResolver()
//                        .getResources(env.getProperty("mybatis.mapperLocations")));
        return sqlSessionFactoryBean;
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

}
