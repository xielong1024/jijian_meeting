package com.gzkit.jijianmeeting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.gzkit.jijianmeeting.dao")
@ServletComponentScan
public class JiJianMeetingApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiJianMeetingApplication.class, args);
    }
}