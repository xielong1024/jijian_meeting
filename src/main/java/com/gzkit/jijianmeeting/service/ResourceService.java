package com.gzkit.jijianmeeting.service;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.entity.ResourceEntity;

import java.util.List;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
public interface ResourceService {

    //判断会议室是否存在
    boolean isExistResourceName(String resourceName);

    /**
     *更新会议室信息
     * @param resourceEntity
     * @return
     */
    void updateResource(ResourceEntity resourceEntity);

    PageInfos<ResourceEntity> getList(int page, int rows);

    //获取可用的会议室
    List<ResourceEntity> getAccessibleResources();


}
