package com.gzkit.jijianmeeting.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.PageUtils;
import com.gzkit.jijianmeeting.dao.ResourceMapper;
import com.gzkit.jijianmeeting.entity.ResourceEntity;
import com.gzkit.jijianmeeting.service.ResourceService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Service(value = "resourceService")
public class ResourceServiceImpl implements ResourceService {
    @Resource(name = "resourceMapper")
    private ResourceMapper resourceMapper;

    @Override
    public boolean isExistResourceName(String resourceName) {
        Example example = new Example(ResourceEntity.class);
        example.createCriteria()
                .andEqualTo("resourceName", resourceName);
        try {
           ResourceEntity resourceEntity= resourceMapper.selectOneByExample(example);
            if (resourceEntity != null) {
               return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public void updateResource(ResourceEntity resourceEntity) {
        resourceMapper.updateByPrimaryKeySelective(resourceEntity);
    }

    @Override
    public PageInfos<ResourceEntity> getList(int page, int rows) {
        PageHelper.startPage(page,rows);
        List<ResourceEntity> list=resourceMapper.selectAll();
        PageInfo<ResourceEntity> resourcePageInfo = new PageInfo<>(list);
        return PageUtils.convert(resourcePageInfo);
    }

    @Override
    public List<ResourceEntity> getAccessibleResources() {
        Example example=new Example(ResourceEntity.class);
        example.createCriteria().andEqualTo("status","1");
        List<ResourceEntity> list=resourceMapper.selectByExample(example);
        return list;
    }
}
