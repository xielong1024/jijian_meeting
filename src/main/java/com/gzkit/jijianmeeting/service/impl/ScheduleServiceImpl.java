package com.gzkit.jijianmeeting.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.PageUtils;
import com.gzkit.jijianmeeting.dao.ScheduleMapper;
import com.gzkit.jijianmeeting.entity.ResourceEntity;
import com.gzkit.jijianmeeting.entity.ScheduleEntity;
import com.gzkit.jijianmeeting.service.ResourceService;
import com.gzkit.jijianmeeting.service.ScheduleService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Service(value = "scheduleService")
public class ScheduleServiceImpl implements ScheduleService {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat sdf1 = new SimpleDateFormat("HH");
    @Resource(name = "scheduleMapper")
    private ScheduleMapper scheduleMapper;

    @Resource(name = "resourceService")
    private ResourceService resourceService;

    @Override
    public List<Map<String, Object>> getDates(Date currentDate) {
        int b = currentDate.getDay();
        Date fdate;
        List<String> list = new ArrayList<String>();
        Long fTime = currentDate.getTime() - b * 24 * 3600000;
        for (int a = 1; a <= 7; a++) {
            fdate = new Date();
            fdate.setTime(fTime + (a * 24 * 3600000)); //一周从周日开始算，则使用此方式
            //fdate.setTime(fTime + ((a-1) * 24*3600000)); //一周从周一开始算，则使用此方式
            list.add(a - 1, sdf.format(fdate));
        }
        int k = 0;
        List<Map<String, Object>> dates = new ArrayList<Map<String, Object>>();
        for (String date : list) {
            Map map = new HashMap();
            Map map1 = new HashMap();
            map.put("date", date);
            map.put("zone", "AM");
            map1.put("date", date);
            map1.put("zone", "PM");
            dates.add(k, map);
            dates.add(k + 1, map1);
            k += 2;
            map = null;
            map1 = null;
        }
        return dates;
    }

    //获取周一和周日时间
    private List<String> getMondayAndSunday(Date currentDate) {
        int b = currentDate.getDay();
        Date mdate = new Date();
        ;
        Date sdate = new Date();
        ;
        List<String> list = new ArrayList<String>();
        Long fTime = currentDate.getTime() - b * 24 * 3600000;
        mdate.setTime(fTime + (1 * 24 * 3600000));
        list.add(sdf.format(mdate));
        sdate.setTime(fTime + (7 * 24 * 3600000));
        list.add(sdf.format(sdate));
        return list;
    }

    @Override
    public boolean isExistBookTime(String time, String zone, Integer resourceId) {
        Example example = new Example(ScheduleEntity.class);
        example.createCriteria().andEqualTo("bookTime", time).andEqualTo("zone", zone)
                .andEqualTo("resourceId", resourceId);
        ScheduleEntity scheduleEntity = scheduleMapper.selectOneByExample(example);
        example = null;
        if (scheduleEntity != null) {
            return true;
        }
        return false;
    }

    @Override
    public PageInfos<ScheduleEntity> getList(int page, int rows, ScheduleEntity scheduleEntity) {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("orderBookDate", sdf.parse(sdf.format(new Date())));
            map.put("resourceId", scheduleEntity.getResourceId());
            map.put("meetingName", scheduleEntity.getMeetingName());
            map.put("bookTime", scheduleEntity.getBookTime());
            map.put("userName", scheduleEntity.getUserName());
            PageHelper.startPage(page, rows);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List list = scheduleMapper.getList( map);
        // List<ScheduleEntity> list = scheduleMapper.selectByExample(example);
        PageInfo<ScheduleEntity> schedulePageInfo = new PageInfo<>(list);
        return PageUtils.convert(schedulePageInfo);
    }

    @Override
    public List<Map<String, List<ScheduleEntity>>> showCurrenrResourceOccupation(int page, int rows) {
        PageHelper.startPage(page, rows);
        Date date = new Date();
        String hour = sdf1.format(date);
        String zone = "";
        if (Integer.parseInt(hour) < 12) {
            zone = "AM";
        } else {
            zone = "PM";
        }
        List<Map<String, List<ScheduleEntity>>> lists = new ArrayList<Map<String, List<ScheduleEntity>>>();
        List<ResourceEntity> resourceEntities = resourceService.getAccessibleResources();
        for (ResourceEntity r : resourceEntities) {
            Map<String, List<ScheduleEntity>> map = new HashMap<String, List<ScheduleEntity>>();
            Example example = new Example(ScheduleEntity.class);
            example.createCriteria().andEqualTo("bookTime", sdf.format(date)).andEqualTo("zone", zone)
                    .andEqualTo("resourceId", r.getId());
            List<ScheduleEntity> list = scheduleMapper.selectByExample(example);
            if (list.size() > 0) {
                map.put("resource", list);
                lists.add(map);
            }
            map = null;
            example = null;
        }
        return lists;
    }

    @Override
    public List<Map<String, List<ScheduleEntity>>> showCurrrentSchedules() {
        List<String> mondayAndSunday = getMondayAndSunday(new Date());
        List<Map<String, List<ScheduleEntity>>> lists = new ArrayList<Map<String, List<ScheduleEntity>>>();
        List<ResourceEntity> resourceEntities = resourceService.getAccessibleResources();
        for (ResourceEntity r : resourceEntities) {
            Example example = new Example(ScheduleEntity.class);
            Map<String, List<ScheduleEntity>> map = new HashMap<String, List<ScheduleEntity>>();
            example.setOrderByClause("order_book_date,zone asc");
            example.createCriteria().andBetween("bookTime", mondayAndSunday.get(0), mondayAndSunday.get(1))
                    .andEqualTo("resourceId", r.getId());
            List<ScheduleEntity> list = scheduleMapper.selectByExample(example);
            if (list.size() > 0) {
                map.put("resource", list);
                lists.add(map);
            }else{
                ScheduleEntity scheduleEntity=new ScheduleEntity();
                scheduleEntity.setResourceId(r.getId());
                scheduleEntity.setResourceName(r.getResourceName());
                list.add(scheduleEntity);
                map.put("resource", list);
                lists.add(map);
            }
            example = null;
            map = null;
        }
        return lists;
    }

    public static int getWeekDay(String day) throws Exception {
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(day));
        int weekday = c.get(Calendar.DAY_OF_WEEK);
        if (weekday == 1) {
            return 7;
        } else {
            return weekday - 1;
        }
    }

}
