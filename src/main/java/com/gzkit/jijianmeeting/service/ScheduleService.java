package com.gzkit.jijianmeeting.service;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.entity.ScheduleEntity;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
public interface ScheduleService {
    /**
     * 获取一周时间
     *
     * @param currentDate
     * @return
     */
    List<Map<String, Object>> getDates(Date currentDate);




    /**
     * 判断是否已预约改时间段
     *
     * @param time
     * @param zone
     * @return
     */
    boolean isExistBookTime(String time, String zone,Integer resourceId);

    //获取会议室预约信息
    PageInfos<ScheduleEntity> getList(int page, int rows,ScheduleEntity scheduleEntity);

    //显示当前会议
     List<Map<String,List<ScheduleEntity>>> showCurrenrResourceOccupation(int page, int rows);

    //显示当周会议安排
    List<Map<String,List<ScheduleEntity>>> showCurrrentSchedules();


}
