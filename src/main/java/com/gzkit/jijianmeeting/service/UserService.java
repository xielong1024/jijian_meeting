package com.gzkit.jijianmeeting.service;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.entity.UserEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
public interface UserService {

    PageInfos<UserEntity> getUserEntities(int page,int rows);

    //用户登录
    boolean login(String account, String password, HttpServletRequest request);

    //判断用户是否存在
    boolean isExistUser(String account);


}
