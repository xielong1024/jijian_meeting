package com.gzkit.jijianmeeting.api;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.JkReturnJson;
import com.gzkit.jijianmeeting.entity.ScheduleEntity;
import com.gzkit.jijianmeeting.service.ScheduleService;
import jdk.nashorn.internal.ir.ReturnNode;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/23 0023
 */
@RestController
@Scope(value = "prototype")
@RequestMapping(value = "/scheduleApi")
public class ScheduleApi {
    @Resource(name = "scheduleService")
    private ScheduleService scheduleService;

    /**
     *显示当前会议
     * @param request
     * @return
     */
    @RequestMapping("/showCurrenrResourceOccupation")
    public JkReturnJson showCurrenrResourceOccupation(HttpServletRequest request){
        JkReturnJson json=new JkReturnJson();
        try{
            String pageSize=request.getParameter("page");
            String rowsSize=request.getParameter("rows");
            int page=pageSize==null?1:Integer.valueOf(pageSize);
            int rows=rowsSize==null?10:Integer.valueOf(rowsSize);
            List<Map<String,List<ScheduleEntity>>> lists=scheduleService.showCurrenrResourceOccupation(page,rows);
            json.setData(lists);
        }catch(Exception e){
            e.printStackTrace();
        }
        return json;
    }

    /**
     *显示当周会议室安排
     * @param request
     * @return
     */
    @RequestMapping("/showCurrrentSchedules")
    public JkReturnJson showCurrrentSchedules(HttpServletRequest request){
        JkReturnJson json=new JkReturnJson();
        try{
            List<Map<String,List<ScheduleEntity>>> lists=scheduleService.showCurrrentSchedules();
            json.setData(lists);
        }catch(Exception e){
            e.printStackTrace();
        }
        return json;
    }
}
