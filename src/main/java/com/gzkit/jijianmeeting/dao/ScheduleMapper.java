package com.gzkit.jijianmeeting.dao;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.MyMapper;
import com.gzkit.jijianmeeting.entity.ScheduleEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Repository(value = "scheduleMapper")
public interface ScheduleMapper extends MyMapper<ScheduleEntity> {
    List<ScheduleEntity> getList(Map map);
}
