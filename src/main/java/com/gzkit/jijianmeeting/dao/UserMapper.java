package com.gzkit.jijianmeeting.dao;

import com.gzkit.jijianmeeting.common.PageInfos;
import com.gzkit.jijianmeeting.common.util.MyMapper;
import com.gzkit.jijianmeeting.entity.UserEntity;
import org.springframework.stereotype.Repository;

import javax.lang.model.element.Name;
import java.util.List;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
@Repository(value = "userMapper")
public interface UserMapper extends MyMapper<UserEntity> {
    public List<UserEntity> getList();
}
