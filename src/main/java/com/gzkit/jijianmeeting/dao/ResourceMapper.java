package com.gzkit.jijianmeeting.dao;

import com.gzkit.jijianmeeting.common.util.MyMapper;
import com.gzkit.jijianmeeting.entity.ResourceEntity;

/**
 * @author xielong
 * @Title:
 * @Description:
 * @date 2018/1/22 0022
 */
public interface ResourceMapper extends MyMapper<ResourceEntity> {

    ResourceEntity selectResource(Integer id);

    int updateResource(ResourceEntity resourceEntity);

}
