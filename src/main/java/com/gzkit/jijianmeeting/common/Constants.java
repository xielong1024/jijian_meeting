package com.gzkit.jijianmeeting.common;


/**
 * 常亮类
 */
public class Constants {




    public static class BUSINESS_CODE{
        public static final int CODE_SUCCESS =200;
        public static final int CODE_BUSINESS_ERROR = 500001;
        public static final int CODE_LACK_PARAMS=500002;

        public static final String USER_SUCCESS_MSG = "操作成功";
        public static final String USER_FAILE_MSG = "操作失败";
        public static final String USER_PARAMS_MSG = "缺少参数";

        public static final String SYS_SUCCESS_MGS="success";
        public static final String SYS_FAIL_MGS="fail";
        public static final String SYS_PARAMS_MSG = "need params";

    }
}
