package com.gzkit.jijianmeeting.common;

public class BaseResult<T> {

    private int statusCode = Constants.BUSINESS_CODE.CODE_SUCCESS;

    private String statusMsg = Constants.BUSINESS_CODE.SYS_SUCCESS_MGS;

    private String userMsg = Constants.BUSINESS_CODE.USER_SUCCESS_MSG;

    private T data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    /**
     * 构建一个简单的出错提示
     * @param userMsg
     * @param e
     * @return
     */
    public static <T>BaseResult<T> buildFail(String userMsg,Exception e){
        BaseResult<T> result = new BaseResult<T>();
        result.setUserMsg(userMsg);
        result.setStatusCode(Constants.BUSINESS_CODE.CODE_BUSINESS_ERROR);
        result.setStatusMsg(e.getMessage());
        return result;
    }
}
