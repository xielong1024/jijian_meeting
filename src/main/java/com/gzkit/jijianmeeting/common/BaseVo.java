package com.gzkit.jijianmeeting.common;

import javax.persistence.Transient;


/**
 * 基类实体
 */
public class BaseVo {


    @Transient
    private  Integer page=1;


    @Transient
    private  Integer rows=10;



    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

}
