package com.gzkit.jijianmeeting.common.util;

import com.github.pagehelper.PageInfo;
import com.gzkit.jijianmeeting.common.PageInfos;


/**
 * 将pagerHelper的分页内容减少,去除过多冗余字段
 */
public class PageUtils {


    public static <T> PageInfos<T> convert(PageInfo<T> pageInfo){
        PageInfos<T> pageInfos = new PageInfos<>();
        pageInfos.setData(pageInfo.getList());
        pageInfos.setPageNo(pageInfo.getPageNum());
        pageInfos.setPageSize(pageInfo.getPageSize());
        pageInfos.setPageCount(pageInfo.getPages());
        pageInfos.setTotalCount(pageInfo.getTotal());
        return pageInfos;
    }
}
