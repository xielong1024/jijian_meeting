package com.gzkit.jijianmeeting.common.util;


import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 请求相关工具类
 */
public class RequestUtils {

    private RequestUtils(){}


    public static int getPage(Integer page){
        return page==null?1:page;
    }

    public static int getRows(Integer rows){
        return rows==null?10:rows;
    }


    /**
     * 将get请求的参数封装成map
     * @param request
     * @return
     */
    public static Map<String, Object> getRequestParams(HttpServletRequest request) {
        Map<String,Object> params  = new HashMap<>();
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String element = parameterNames.nextElement();
            String parameter = request.getParameter(element);
            params.put(element,parameter);
        }
        return params;
    }


}
