package com.gzkit.jijianmeeting.common.util;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;


/**
 * 通用Mapper
 * @param <T>
 */
public interface MyMapper<T> extends Mapper<T>,MySqlMapper<T> {
}
