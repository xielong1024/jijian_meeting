package com.gzkit.jijianmeeting.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 工具类
 */
public class MD5Util {
	/**
	 *
	 * @param val
	 *            待转换的字符串
	 * @param salt
	 *            盐
	 * @param encryptTimes
	 *            循环次数
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String getMD5(String val, String salt, int encryptTimes) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(val.getBytes());
			byte b[] = md5.digest();
			int i;
			StringBuffer sb = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0) {
					i += 256;
				}
				if (i < 16) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(i));
			}
			if (encryptTimes <= 0) {
				return sb.toString();
			}
			if (salt != null && !"".equals(salt)) {
				return getMD5(sb.toString() + salt, salt, --encryptTimes);
			} else {
				return getMD5(sb.toString(), salt, --encryptTimes);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			return val;
		}
	}

}
